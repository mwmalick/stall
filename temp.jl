include("util.jl")

function asshole(files, i, fstring = "-")
    for f in files
        cmd = load_data(f)
        plot(cmd[:,i], cmd[:,3], fstring)
    end
end
files = freg(rd("data/favier/alpha9k135-drag/"), r"phi\d*$")
temps = freg(rd("data/favier/alpha9k135-drag/tempshit"), r"phi\d*$")

function farmboy(dir, h2,a)
    s = init_case(dir)
  s.case[:a] = a
  s.case[:h2] = h2
  normal_setup(s)
  ds = make_primarystall(s)
  ps = zeros(2,6)
  t,theta,cm = ds(ps)
  plot(t,cm)
end
function revdata(files)
    for f in files
	cmd = load_data(f)
	theta = reverse!(cmd[:,2])
	c = reverse!(cmd[:,3])
	open("$f","w") do x
	    for i in 1:length(c)
		t = theta[i]
		load = c[i]
		write(x, "$t\t$load\n")
	    end
	end
    end
end

function officer(casefile)
    s = init_case(casefile)
    normal_setup(s)
    ds = make_primarystall(s)
    ps = zeros(2,6)
    t, theta, cm = ds(ps)
    θs = s.case[:θ_funcs]
    k = s.case[:k]
    dc = s.outputs[:ΔC]
    θ = θs[1]
    cmd = s.case[:cmd]
    pd = s.case[:profile_drag]
    plot(t, theta, cmd[:,1], cmd[:,3],"-*", t, θ(t*k),"--", t, pd-dc(theta))
    figure()
    plot(cmd[:,2], cmd[:,3], theta, pd-dc(theta))
end
casefiles = rd("mastercases/favier/alpha9k135-drag/")
casefiles = freg(casefiles, r"jl$")
casefile = "mastercases/favier/alpha9k135-drag/phi0.jl"
s = init_case(casefile)
s2 = init_case(casefile)
normal_setup(s)
unsteady_setup(s2)
ds = make_primarystall(s)
ds2 = make_unsteadyfreestreamstall(s2)
ps = s.case[:initial_points] |> change_params
t,theta,cm = ds(ps)
t2,theta2,cm2 = ds2(ps)
lam1 = s.outputs[:x][:,3:10]
lam2 = s2.outputs[:x][:,3:10]
bvect = s.matrices[:bvect]
λ01 = [.5(lam1[[i],:]*bvect)[1] for i in 1:1800]
λ02 = [.5(lam2[[i],:]*bvect)[1] for i in 1:1800]
longt = s.outputs[:t]
a1,a1_ = fuck(s, s.outputs[:u0fun], (t) -> 0.0)
a2, a2_ = fuck(s2, s2.outputs[:u0fun], s2.outputs[:u0_fun])
@shit(s.outputs, (ΔC, ΔC_))
w01(t) = s.outputs[:w](t)[1]
w02(t) = s2.outputs[:w](t)[1]
u01 = s.outputs[:u0fun]
u02 = s2.outputs[:u0fun]
θ, θ_, θ__ = s.case[:θ_funcs]
ΔC = s.outputs[:ΔC]
clf()
plot(longt, a1, longt, a2, longt, θ(longt))
yticks(collect(0:.05:.3))
