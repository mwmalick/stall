using Allstall
import SerializeCases

include("util.jl")
files = freg(rd("mastercases/vr7/"), r"vr7[^/]*(moment|drag)\.jl$")
function dict_eq_helper(a::Dict, b::Dict)
    ax = setdiff(keys(a), keys(b))
    bx =  setdiff(keys(b), keys(a))
    allkeys = intersect(keys(a), keys(b))
    badkeys = []
    for k in allkeys
	if a[k]!=b[k]
	    push!(badkeys, k)
	end
    end
    return badkeys, ax, bx
end
function dict_eq(a::Dict, b::Dict)
    bad, ax, bx = dict_eq_helper(a,b)
    return isempty(bad) && isempty(ax) && isempty(bx)
end
function test_serialization(casefile::AbstractString)
    s = init_case(casefile)
    normal_setup(s)
    ds = make_primarystall(s)
    ps = rand(2,6)
    t,theta,cm = ds(ps)
    open("q.ser","w") do x
        serialize(x,s)
    end
    s2 = open(deserialize, "q.ser")
    @assert s.case == s2.case
    #@assert s.outputs == s2.outputs
    @assert(keys(s.outputs) == keys(s2.outputs))
    for k in keys(s.outputs)
	@assert(s.outputs[k] == s2.outputs[k], "s.outputs[$k]")
    end

    ds2 = make_primarystall(s2)
    t2,theta2,cm2 = ds2(ps)
    @assert t==t2
    @assert theta2 == theta
    @assert cm == cm2
end
