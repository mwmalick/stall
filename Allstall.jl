__precompile__()
module Allstall
export Slacker, make_primarystall, normal_setup, change_params, defaultcost, shotgun, savedata, radius_norm_error, normal_error, primarystall, make_unsteadyfreestreamstall, unsteady_setup, eta_constraint, @shit, @rshit, secondary_loads, datafile_formatting, load_data, assume_θ, ΔCl_funcs, maxval_cost, multi_cost
export secondary_cost, make_shortstall, short_setup, constrained_error

macro rshit(args...)
    pairs = []
    for x in args
	y=x
	push!(pairs, ":$x => $y")
    end
    p = join(pairs,",")
    esc(parse("Dict($p)"))
end
#This will expand Dict(:x=>1, :y=>5) into local variables x=1, y=5.
#called like (dict, (x,y,z...))
macro shit(d,x)
    r = quote end
    for s in x.args
	s2 = "$s"
	push!(r.args, esc(quote
			      s3 = $s2
			      $s = $d[Symbol(s3)]
			  end))
    end
    r
end
type Slacker
    case::Dict
    matrices::Dict
    plotoptions::Dict
    optparams::Dict
    outputs::Dict
    Slacker(m::Dict) = new(m, Dict(), Dict(), Dict(), Dict())
end

function drag(h,h_,h__,v,v_,λ₀,λ₁,u₀,u₀_,One,b,C,G,K,M,ρ,S,H)
    hnλ = h_+v-λ₀
    (2π*ρ*( -b*hnλ'*S*hnλ + b*(h__+v_)'*G*h - u₀*hnλ'*(K-H)*h + (u₀_*h-u₀*v + u₀*λ₀)'*H*h ))[1]
end
Γ_linear(h,h_,v,λ₁,u₀,One,b,C,G,K) = 2π*( b*One'*(C-G)*(h_ + v - λ₁) + u₀*One'*K*h )
L_linear(h,h_,h__,v,v_,λ₀,u₀,u₀_,b,C,G,K,M,ρ) = 2π*ρ*(-b^2*M*(h__+v_) - b*u₀*C*(h_ + v - λ₀) - u₀^2*K*h - b*G*(u₀_*h - u₀*v + u₀*λ₀))
function L0_linear{T<:AbstractFloat}(w::Vector{T}, w_::Vector{T}, λ0::T, u0::T, b::T, ρ::T, f::T)
    w0, w1 = w[1:2]
    w0_, w2_ = w_[1], w_[3]
    -2π*ρ*b*f*u0*(w0-λ0) - π*ρ*b*u0*w1 - π*ρ*b^2*(w0_ - .5w2_)
end
function inflow{T<:AbstractFloat}(Ainv::Matrix{T}, c::Vector{T}, etrans::Matrix{T}, b::T, u0fun::Function, u0_fun::Function, ftrans::Matrix{T}, v_::Function, h::Function, h_::Function, h__::Function, n::Integer)
    b2 = 2pi*b
    function λ_(t::T,x::Array{T,2})
	u0 = u0fun(t)
	u0_ = u0_fun(t)
    	Γ_ = x[2]
    	λ = x[3:n+2]
	ret = Ainv*( c*(etrans*(h__(t) + v_(t)) + u0/b*ftrans*h_(t) + u0_/b*ftrans*h(t) + Γ_/b2)[1] - u0/b*λ)
	return ret'
    end
    return λ_
end
function stall3{T<:AbstractFloat}(b::T, U::T, parameter_function::Function, ΔC::Function, ΔC_::Function, ΔCl::Function, w::Function, w_::Function, n::Integer, u0fun::Function, u0_fun::Function, bvect::Vector{T}; αs=0.0)
    w0(t) = w(t)[1]
    w0_(t) = w_(t)[1]
    function Γ__(t::T,x::Array{T,2}, p::Matrix{T})
	λ = x[[1],3:n+2]
	λ0 = .5(λ*bvect)[1]
	λ0_ = 0.0
	u0 = u0fun(t)
	u0_ = u0_fun(t)
	α = (w0(t)-λ0)/u0 - αs
	α_ = α_fun(w0(t), λ0, u0, w0_(t), λ0_, u0_)
	cl = ΔCl(α)
	stall_params = parameter_function(t,p)
	e = stall_params[1] + stall_params[2]*cl^2
	η = stall_params[3] + stall_params[4]*cl^2
	ω = stall_params[5] + stall_params[6]*cl^2
    	(Γ, Γ_) = x[1:2]
	-(U/b)^2*( η*b/U*Γ_ + ω^2*Γ + b*u0*ω^2*( ΔC(α) + e*ΔC_(α)*b/U*α_) )
    end
    return (t::T,x::Array{T,2})->x[2], Γ__
end
function stall_fake_lambda0{T<:AbstractFloat}(ΔC::Vector{T}, ΔC_::Vector{T}, cl2::Vector{T}, α_::Vector{T}, halfway_index::Int)
    function Γindexed(i::Int, x::Matrix{T}, p::Matrix{T})
	j = 1 + div(i, halfway_index)&1 
	stall_params = p[j,:]
	e = stall_params[1] + stall_params[2]*cl2[i]
	η = stall_params[3] + stall_params[4]*cl2[i]
	ω2 = (stall_params[5] + stall_params[6]*cl2[i])^2
    	(Γ, Γ_) = x[1:2]
	return [x[2] -(η*Γ_ + ω2*Γ + ω2*(ΔC[i] + e*ΔC_[i]*α_[i]))]
    end
end

asin_rounded(a) = -1<a<1 ? asin(a) : asin(round(a))
#assuming theta is a sin function fit of the given data.
#adding h2, (needs a more robust way of doing this)
function shape_functions{Fl<:AbstractFloat}(fns::Vector{Function}, u0fun::Function, u0_fun::Function, W::Matrix{Fl}, I::Matrix{Fl}, T::Vector{Fl}, h2::Fl, fa=1)
    θ, θ_, θ__ = fns
    h(t) = T*fa*θ(t) + [0;0;h2]
    h_(t) = T*θ_(t)
    h__(t) = T*θ__(t) 
    w(t) = u0fun(t)*W*h(t) + I*h_(t)
    w_(t) = u0fun(t)*W*h_(t) + u0_fun(t)*W*h(t) + I*h__(t)
    return h,h_,h__,w,w_
end
function stall_parameter_funcs{T<:AbstractFloat}(ranges::Vector{T}, cycle::T)
    (t,p::Matrix{T})->p[t%cycle.>ranges, :][end,:]
end
function ΔCl_funcs(cl::Tuple{Array{Float64,1}, Array{Float64,1}}; smooth=true, n=10000, harmonics = 100)
    cl1,cl2 = cl
    if smooth
	x,y = cl1,cl2
	x = [x; reverse(2max(x...)-x)]
	y = [y; reverse(y)]
	xsig = linspace(min(x...), max(x...), n) |> collect
	sig = interp(x,y,xsig)
	fsig = fft(sig)
	fsig[harmonics:end-harmonics] = 0
	n2 = n/2 |> Int
	sig = real(ifft(fsig))[1:n2]
	x,y = xsig[1:n2], sig
	cl1,cl2 = x,y
    end
    g = gradient(cl2,cl1)
    return ((x) -> interp(cl1, cl2, x)) , (x) -> (interp(cl1, g, x))
end
αfun{T<:AbstractFloat}(w0::T, λ0::T, u0::T) = atan2(w0-λ0, u0)
α_fun(w0::AbstractFloat, λ0::AbstractFloat, u0::AbstractFloat, w0_::AbstractFloat, λ0_::AbstractFloat, u0_::AbstractFloat) = 1/(1+((w0-λ0)/u0)^2)*((w0_ - λ0_)/u0 + u0_/u0^2*(w0-λ0))
α_fun(w0, λ0, u0, w0_, λ0_, u0_) = 1 ./ (1+((w0-λ0)./u0).^2).*((w0_ - λ0_)./u0 + u0_./u0.^2 .* (w0-λ0))

function parse_casefile(casefile::AbstractString)
    include(casefile)
    return Slacker(evaluate_case())
end
function normal_setup(s::Slacker, short=false)
    @shit(s.case, (f, n, m, a, u, k, θ_funcs, ΔC_static, ΔCl_static, a, b, ρ, u_avg, cycles, resolution, h2, αs, ranges))
    const A,Ainv,bvect,c,C,C1,C2,evect,fvect,G,H,I,K,M,mvect,One,S,W,Tm = defineMatrices(f,n,m,a)
    s.matrices = @rshit(A,Ainv,bvect,c,C,C1,C2,evect,fvect,G,H,I,K,M,mvect,One,S,W,Tm)
    u₀, v₀, v₁ = u
    u0fun = (t) -> u₀
    u0_fun = (t) -> 0.0
    v_(t) = 0.0
    v(t) = [v₀; v₁; zeros(m-1)]
    θ, θ_, θ__ = θ_funcs
    θstatic, ΔC_static = ΔC_static[:,1], ΔC_static[:,2]
    θstatic = max(θstatic...) > pi ? θstatic*pi/180 : θstatic
    θstatic_cl, ΔCl_static = ΔCl_static[:,1], ΔCl_static[:,2]
    θstatic_cl = max(θstatic_cl...) > pi ? θstatic_cl*pi/180 : θstatic_cl
    ΔC, ΔC_ = ΔCl_funcs((θstatic, ΔC_static))
    ΔCl, ΔCl_ = ΔCl_funcs((θstatic_cl, ΔCl_static))
    h, h_, h__, w, w_ = shape_functions(θ_funcs, u0fun, u0_fun, W, I, Tm, h2)
    λ_ = inflow(Ainv, c, evect', b, u0fun, u0_fun, fvect', v_, h, h_,h__, n)
    t = 0 : 2pi/k/resolution: 2pi*cycles/k
    t = linspace(0,2pi*cycles/k, resolution*cycles)
    x0 = zeros(n+2)
    Γ_f, Γ__f = stall3(b, u_avg, stall_parameter_funcs(ranges, 2pi/k), ΔC, ΔC_, ΔCl, w, w_, n, u0fun, u0_fun, bvect; αs = αs)
    s.outputs = @rshit(ΔC, ΔCl, ΔC_, ΔCl_, h, h_, h__, u0fun, u0_fun, v, v_, w, w_, λ_, t, x0, Γ_f, Γ__f)
end
function short_setup(s::Slacker)
    @shit(s.case, (f, n, m, a, u, k, θ_funcs, ΔC_static, ΔCl_static, a, b, ρ, u_avg, cycles, resolution, h2, αs, ranges))
    #resolution = 2*s.case[:resolution] 
    original_resolution = s.case[:resolution]
    s.case[:resolution] *= 2
    normal_setup(s)
    ds = make_primarystall(s)
    p = s.case[:initial_points] |> change_params
    ds(p)
    @shit(s.outputs, (w, w_, x, u0fun, ΔC, ΔCl, ΔC_, t))
    bvect = s.matrices[:bvect]
    αs = s.case[:αs]
    w0fun = t->w(t)[1]
    w0_fun = t->w_(t)[1]
    w0::Vector{Float64} = map(w0fun, t)
    w0_::Vector{Float64} = map(w0_fun, t)
    λ = x[:,3:n+2]
    λ0::Vector{Float64} = .5(λ*bvect) |> vec
    λ0_ = 0.0
    u0::Vector{Float64}  = map(u0fun,t)
    u0_fun = (t) -> 0.0
    u0_::Vector{Float64}  = map(u0_fun,t)
    α::Vector{Float64}  = (w0-λ0)./u0 - αs
    α_::Vector{Float64} = α_fun(w0, λ0, u0, w0_, λ0_, u0_)
    cl2 = ΔCl(α).^2
    ΔC_array = map(ΔC, α)
    ΔC__array = map(ΔC_, α)
    halfway_index = original_resolution ##ONLY works for upstroke = 0 to pi, downstroke = pi to 2pi
    s.case[:resolution] = original_resolution
    normal_setup(s)
    ds = make_primarystall(s)
    ds(zeros(2,6))
    s.outputs[:α_] = α_
    s.outputs[:ΔC_array] = ΔC_array
    s.outputs[:ΔC__array] = ΔC__array
    s.outputs[:cl2] = cl2
    s.outputs[:Γindexed] = stall_fake_lambda0(ΔC_array, ΔC__array, cl2, α_, halfway_index)
end

function steady_loads(s::Slacker)
    @shit(s.outputs, (x,t,h,h_,h__,w,w_, v, v_, u0fun))
    @shit(s.case, (a, ρ, n, m, quantity, b, f))
    @shit(s.matrices, (C,G,K,M,S,H,bvect,One))
    Γ = x[:,1]
    Γ_= x[:,2]
    λ = x[:,3:n+2]
    C_computed = zeros(length(t))
    for i in 1:length(t)
	ti = t[i]
	w0 = w(ti)[1]
	λ0 = .5(λ[[i],:]*bvect)[1]
	λ0_v = [λ0; zeros(m)]
	u0 = u0fun(ti)
	#α = αfun(w0, λ0, u0)
	#α_ = α_fun(w0, λ0, u0, w0_, λ0_, u0_)
	if quantity == :moment
	    L = L_linear(h(ti),h_(ti),h__(ti),v(ti),v_(ti),λ0_v,u0,0,b,C,G,K,M,ρ)
	    C_computed[i] = ((L[2]-L[1]*a)/2 + ρ*u0*Γ[i])/(ρ*u0^2*b)
	elseif quantity == :drag
	    C_computed[i] = (drag(h(ti),h_(ti),h__(ti),v(ti),v_(ti),λ0,λ[i,1],u0,0.0,One,b,C,G,K,M,ρ,S,H) + 2ρ*u0*Γ[i])/(2*b*u0^2*ρ) + s.case[:profile_drag]
	else #lift
	    L0 = L0_linear(w(ti),w_(ti),λ0,u0,b,ρ,f)
	    C_computed[i] = (-L0+ ρ*u0*Γ[i])
	end
    end
    return C_computed
end
function unsteady_setup(s::Slacker)
    normal_setup(s)
    @shit(s.case, (f, n, m, a, u, k, θ_funcs, ΔC_static, ΔCl_static, a, b, ρ, u_avg, cycles, resolution, h2, αs, ranges))
    @shit(s.case, (phase_ss, u_avg, u_amp, phi, aoa_phase))
    @shit(s.matrices, (A,Ainv,bvect,c,C,C1,C2,evect,fvect,G,H,I,K,M,mvect,One,S,W,Tm))
    @shit(s.outputs, (ΔC, ΔC_, ΔCl, ΔCl_, v, v_))
    θ,θ_,θ__ = θ_funcs
    u0fun = (t) -> u_avg + u_amp*sin(k*t - phi + aoa_phase)
    u0_fun = (t) -> k*u_amp*cos(k*t - phi + aoa_phase)
    h, h_, h__, w, w_ = shape_functions(θ_funcs, u0fun, u0_fun, W, I, Tm, h2)
    λ_ = inflow(Ainv, c, evect', b, u0fun, u0_fun, fvect', v_, h, h_,h__, n)
    Γ_f, Γ__f = stall3(b, u_avg, stall_parameter_funcs(ranges, 2pi/k), ΔC, ΔC_, ΔCl, w, w_, n, u0fun, u0_fun, bvect; αs = αs)
    tss = phase_ss/k
    αss = θ(tss)
    αss_ = θ_(tss)
    Uss = u0fun(tss)
    outputs_temp = @rshit(tss, αss, αss_, Uss, λ_, Γ_f, Γ__f, u0fun, u0_fun, h, h_, h__, w, w_)
    for (key,val) in outputs_temp
	s.outputs[key] = val
    end
end
function unsteady_loads(s::Slacker)
    @shit(s.case, (a, ρ, n, m, u_avg, quantity, b, θ_funcs, ϵ, Cl_static, kappa))
    @shit(s.outputs, (x,t,h,h_,h__,w,w_, v, v_, tss, αss, αss_, Uss, ΔCl, u0fun, u0_fun))
    @shit(s.matrices, (C,G,K,M,S,H,bvect,One))
    θ,θ_,θ__ = θ_funcs
    Γ = x[:,1]
    Γ_= x[:,2]
    λ = x[:,3:n+2]
    C_computed = zeros(length(t))
    for i in 1:length(t)
	ti = t[i]
	w0 = w(ti)[1]
	λ0 = .5(λ[[i],:]*bvect)[1]
	λ0_v = [λ0; zeros(m)]
	u0 = u0fun(ti)
	α = θ(ti) 
	α_ = θ_(ti)
	Uc = U_correction(u0, Uss, α, α_, αss, αss_)
	Up = w0-λ0
	Fc = kappa*ΔCl(ti)/(Cl_static(ti)+ ΔCl(ti))
	clα=5.2
	if quantity == :moment
	    L = L_linear(h(ti),h_(ti),h__(ti),v(ti),v_(ti),λ0_v,u0,0,b,C,G,K,M,ρ)
	    Cn = (L[2]-L[1]*a)*b/2 + ρ*u0*Γ[i] + ρ*Fc*(Uc-u0)*Γ[i] + Fc*(u0-Uc)*ρ*a*b^2*Up*ϵ 
	    C_computed[i] = Cn/(ρ*u0^2*b^2)
	elseif quantity == :drag
	    d = drag(h(ti),h_(ti),h__(ti),v(ti),v_(ti),λ0,λ[i,1],u0,0.0,One,b,C,G,K,M,ρ,S,H) 
	    C = (d/2 + ρ*u0*Γ[i] + ρ*Fc*(Uc-u0)*Γ[i] + Fc*(u0-Uc)*ρ*a*2b*Up*ϵ)
	    C_computed[i] = C/(b*u0^2*ρ) + s.case[:profile_drag]
	else
	    L0 = L0_linear(w(ti),w_(ti),λ0,u0,b,ρ,f)
	    C_computed[i] = (-L0+ ρ*u0*Γ[i])
	end
    end
    return C_computed
end
stepfun(t) = t < 0 ? 0.0 : 1.0
#NOTE! t is in PHASE for these functions
function secondary_stall{T<:AbstractFloat}(t::T, x::Matrix{T}, params::Matrix{T})
    ζ,ω,h,ti,Δt = params
    u = stepfun
    Γs, Γs_ = x[1:2];
    t = (t)%(2pi)
    Γs__ = -2ζ*ω*Γs_ - ω^2*Γs + ω^2*h*(u(t-ti)-u(t-ti-Δt))
    return [Γs_ Γs__] 
end
function secondary_loads{T<:AbstractFloat}(t::StepRangeLen{T}, params::Matrix{T})
    @assert size(params) == (1,5)
    return rk4(secondary_stall, t, zeros(1,2), params)[:,1]
end
function estimate_ss{T<:AbstractFloat}(x1::T, y1::T, x2::T, y2::T)
    Δt = (x2-x1)/3
    ti = x1-Δt
    w = 2pi/(3Δt)
    ζ = log(y1/y2)/2pi
    h = 1.219y1
    return [ζ w h ti Δt]
end

function U_correction(u0, Uss, α, α_, αss, αss_)
    if α<αss 
	return u0
    #elseif α<αss && α_<0
#	return u0 + (Uss-u0)*abs(α_/αss_)
    else
	return Uss
    end
end
function primarystall(s::Slacker)
    @shit(s.outputs, (Γ_f, Γ__f, λ_, t, x0))
    p = s.case[:initial_points] |> change_params
    rates(t::AbstractFloat, x::Array{Float64,2}, p::Matrix{Float64}) = [ Γ_f(t,x) Γ__f(t,x,p) λ_(t,x) ]
    s.outputs[:x] = rk4(rates,t,x0, p)
    Cn = steady_loads(s)
    s.outputs[:Cn] = Cn
    θ = s.case[:θ_funcs][1]
    k = s.case[:k]
    resolution = s.case[:resolution]
    t_trunc = t[1:resolution]
    return t_trunc*k, θ(t_trunc), Cn[end-resolution+1:end]
end
function make_primarystall(s::Slacker)
    @shit(s.outputs, (Γ_f, Γ__f, λ_, t, x0))
    rates(t::AbstractFloat, x::Array{Float64,2}, p::Matrix{Float64}) = [ Γ_f(t,x) Γ__f(t,x,p) λ_(t,x) ]
    function ds(p::Matrix)
	s.outputs[:x] = rk4(rates,t,x0, p)
	Cn = steady_loads(s)
	s.outputs[:Cn] = Cn
	θ = s.case[:θ_funcs][1]
	k = s.case[:k]
	resolution = s.case[:resolution]
	t_trunc = t[1:resolution]
	return t_trunc*k, θ(t_trunc), Cn[end-resolution+1:end]
    end
    s.outputs[:ds] = ds
    ds
end
function make_unsteadyfreestreamstall(s::Slacker)
    @shit(s.outputs, (Γ_f, Γ__f, λ_, t, x0))
    rates{T<:AbstractFloat}(t::AbstractFloat, x::Array{T,2}, p::Matrix{T}) = [ Γ_f(t,x) Γ__f(t,x,p) λ_(t,x) ]
    function ds(p::Matrix)
	s.outputs[:x] = rk4(rates,t,x0, p)
	Cn = unsteady_loads(s)
	s.outputs[:Cn] = Cn
	θ = s.case[:θ_funcs][1]
	resolution = s.case[:resolution]
	t_trunc = t[1:resolution]
	k = s.case[:k]
	return t_trunc*k, θ(t_trunc), Cn[end-resolution+1:end]
    end
    s.outputs[:ds] = ds
    ds
end
function make_shortstall(s::Slacker)
    @shit(s.outputs, (t, Γindexed))
    x0 = zeros(1,2)
    k = s.case[:k]
    resolution = s.case[:resolution]
    t_trunc = t[1:resolution]
    θ = s.case[:θ_funcs][1]
    function ds(p::Matrix)
	x = rk4_indexed(Γindexed, t, x0, p)
	s.outputs[:x][:,1:2] = x
	Cn = steady_loads(s)
	s.outputs[:Cn] = Cn
	return t_trunc*k, θ(t_trunc), Cn[end-resolution+1:end]
    end
    s.outputs[:ds] = ds
    ds
end
function assume_θ(k, θs)
    avg, amp = (max(θs...)+min(θs...))/2, (max(θs...)-min(θs...))/2
    ph = asin_rounded( (θs[1]-avg)/amp )
    θ(t) = avg+amp*sin.(k*t +ph)
    θ_(t) = amp*k*cos.(k*t +ph)
    θ__(t) = -amp*k^2*sin.(k*t +ph)
    return [θ, θ_, θ__, ph]
end
function defineMatrices(f,n,m,a)
    D = [ abs(x-y)==1 ? (x-y)/2x : 0 for x in 1:n, y in 1:n]
    d = zeros(n,1)
    d[1] = .5
    N = 1:n-1
    bvect = [(-1)^(x+1)*factorial(n+x-1)/factorial(n-x-1)/factorial(x)^2 for x in N]'
    bvect = [bvect (-1)^(n+1)]
    c = 2 ./ (1:n); 
    A::Matrix{Float64} = D + d*bvect + c*d' + .5c*bvect
    Ainv = inv(A)
    C = [ abs(x-y)==1 ? (y-x)/2 : 0 for x in 1:m+1, y in 1:m+1]
    G = C/2; G[1:2,1:2] = [0 .5; 0 0] 
    C[1,1:2] = [f 1]
    C1 = C[:,1]
    C2 = C[:,:]
    C2[:,1] = zeros(m+1);
    evect = zeros(m+1,1); evect[1]=f; evect[2]=.5
    fvect = hcat(collect(0.0:m))
    H = diagm(collect(0:m)/2)
    I = eye(m+1)
    K = zeros(m+1, m+1);
    for row in 1:m+1
        for col in 1:m+1
            if row==1 && rem((col-1)/2,1)>0
                K[row,col] = f*(col-1)
            elseif row==1 && rem((col-1)/2,1)==0
                K[row,col] = col-1
            elseif row==col && row>1
                K[row,col] = -(col-1)/2
            end
        end
    end
    M = zeros(m+1,m+1)
    for x in 1:m+1
	for y in 1:m+1
	    if x==y
		M[x,y] = (x-1)/(4*((x-1)^2-1))
	    elseif y-x == 2
		M[x,y] = -1/(8x)
	    elseif x-y == 2
		M[x,y] = -1/(8y)
	    end
	end
    end
    M[1,1] = .5; M[2,2] = 1/16; 
    if m > 1
        M[1,3] = M[3,1] = -.25;
    end
    mvect=zeros(m+1)
    for i in 2:2:m+1  
        mvect[i,1]=i-1 
    end
    One = [1; zeros(m)]
    S = zeros(m+1,m+1)
    S[1] = f

    W = zeros(m+1,m+1)
    for x in 1:m+1
	for y in x+1:2:m+1
	    W[x,y] = 2(y-1)
	end
    end
    W[1,:] = .5 * W[1,:]
    T = zeros(m+1)
    T[1:2] = [-a,1]
    return A,Ainv,bvect' ,c,C,C1,C2,evect,fvect,G,H,I,K,M,mvect,One,S,W,T
end

function Tmatrix(m,b,a,ϕ)
    T = zeros(m+1,2)
    T[1:2,1] = [-a*b,b]
    T[1,2] = sin.(ϕ) - ϕ*cos(ϕ)
    T[2,2] = ϕ - sin.(ϕ)*cos(ϕ)
    T[3:end,2] = [1/(n+1)*sin.((n+1)*ϕ) + 1/(n-1)*sin.((n-1)*ϕ) - 2/n*cos(ϕ)*sin.(n*ϕ) for n in 3:m+1]
    T[:,2] *= b/pi
    return T
end
#--------------------------------
##4k4 - -------------------------------------------------------------------------
k_func(f,t,x,s,v) = f(t+s/2, x+.5s*v)
k_func(f,t,x,s,v, args...) = f(t+s/2, x+.5s*v, args...)
k_func_indexed(f,i,x,s,v,args...) = f(i, x+.5s*v, args...)
function rk4(f,t::StepRangeLen,x0)
    l = length(t)
    s = t.step
    x = zeros(length(t),length(x0))
    x[1,:] = x0
    for i in 2:l
	x[i,:] = onestep(f,t[i-1],x[[i-1],:],s)
    end
    return x
end
function rk4(f::Function, t::StepRangeLen, x0, args...)
    l = length(t)
    x = zeros(length(t),length(x0))
    x[1,:] = x0
    for i in 2:l
	s = t[i]-t[i-1]
	x[i,:] = onestep(f,t[i-1],x[[i-1],:],s, args...)
    end
    return x
end
function rk4_indexed(f::Function, t::StepRangeLen, x0::Matrix{Float64}, args...)
    l = length(t)
    x = zeros(l,length(x0))
    x[1,:] = x0
    for i in 2:l
	s = t[i]-t[i-1]
	x[i,:] = onestep_indexed(f, i-1, x[[i-1],:], s, args...)
    end
    return x
end
function onestep(f::Function, t::Float64, x::Matrix{Float64}, s::Float64)
    k1::Matrix{Float64} = k_func(f,t,x,0,0)
    k2::Matrix{Float64} = k_func(f,t,x,s,k1)
    k3::Matrix{Float64} = k_func(f,t,x,s,k2)
    k4::Matrix{Float64} = k_func(f,t,x,2s,k3)
    return  s*(k1+2k2+2k3+k4)/6 + x
end
function onestep(f,t,x,s, args...)
    k1 = k_func(f,t,x,0,0, args...)
    k2 = k_func(f,t,x,s,k1, args...)
    k3 = k_func(f,t,x,s,k2, args...)
    k4 = k_func(f,t,x,2s,k3, args...)
    return  s*(k1+2k2+2k3+k4)/6 + x
end
function onestep_indexed(f::Function, i::Int, x::Matrix{Float64}, s::Float64, args...)
    k1 = k_func_indexed(f,2i,x,0,0, args...)
    k2 = k_func_indexed(f,2i+1,x,s,k1, args...)
    k3 = k_func_indexed(f,2i+1,x,s,k2, args...)
    k4 = k_func_indexed(f,2i+2,x,2s,k3, args...)
    return s*(k1+2k2+2k3+k4)/6 + x
end
function rk4!{T<:AbstractFloat}(f::Function, x::Matrix{T}, t::StepRangeLen, args...)
    l = length(t)
    s = t.step
    for i in 2:l
	x[i, :] = onestep(f, t[i-1], x[[i-1], :], s, args...)
    end
    x
end
#-----Optimization and cost functions below
function shotgun{T<:AbstractFloat}(f::Function, N::Int, n::Int, iterations::Int, minpoint::Matrix{T}, maxpoint::Matrix{T}; tolerance=.005, scaling=1e-5^(1/iterations), startpoints=nothing)
    N = N-N%n
    n2 = round(Int, N/n)-1
    d = maxpoint-minpoint
    #points = SharedArray(T,N,length(d))
    points::Matrix{T} = minpoint.*ones(N) + rand(N, length(d)).*d
    if typeof(startpoints) == Matrix{T}
	rows,cols = size(startpoints)
	points[1:rows,1:cols] = startpoints
    end
    fs = SharedArray{T}(N,2)
    for i in 1:iterations
	@time begin
	print("iteration # $i\n")
	@sync @parallel for i in 1:N
	    fs[i,:] = [f(points[[i],:]), i]
	end
	topn = convert(Vector{Int}, sortrows(fs)[1:n,2])
	print("Best point cost: $(fs[topn[1],1])\n")
	points[1:n, :] = points[topn,:]
	d = scaling*d
	for k in 1:n
	    p = points[[k],:]
	    mn = max.(minpoint, p-.5d)
	    mx = min.(maxpoint, p+.5d)
	    m = mn.*ones(n2) + rand(n2,length(p)).*(mx-mn)
	    points[n+1+(k-1)*n2:n+k*n2, :] =  m
	end
    end
    end
    return points
end
#=
function vectorizerows(f::Function, T::Type=Float64)
    function newf(x::Matrix{T})
	r,c = size(x)
	ret = T[f(x[i,:]) for i in 1:r]
	return r>1 ? ret : ret[1]
    end
    return newf
end
=#

#primary stall cost, any norm you want
function defaultcost(s::Slacker, ds::Function, error_function::Function, constraint::Function)
    cmd = s.case[:cmd]
    c_data = cmd[:,3]
    td = cmd[:,1]
    function cost(p)
	p = change_params(p)
	t, θ, cl = ds(p)
	t = collect(t)
	return error_function(t,cl,td,c_data) + constraint(p)
    end
    return cost
end
function multi_cost(slackers::Array{Slacker,1})
    costs = Array{Function, 1}()
    for (i,s) in enumerate(slackers)
	push!(costs, defaultcost(s, s.outputs[:ds], normal_error, (t)->0.0))
    end
    cost(t) = sum([f(t) for f in costs])/length(slackers)
    return cost, costs
end
function maxval_cost(s::Slacker, ds::Function, error_function::Function; penalty=10.0, constraint = (t)->0.0)
    cmd = s.case[:cmd]
    maxval = max(cmd[:,3])
    c_data = cmd[:,3]
    td = cmd[:,1]
    function cost(p)
	p = change_params(p)
	t, θ, cl = ds(p)
	m = maximum(cl)
	t = collect(t)
	return error_function(t,cl,td,c_data) + constraint(p) + penalty*abs(maxval-m)
    end
    return cost
end

function secondary_cost(s::Slacker, error_function::Function)
    cmd = s.case[:cmd]
    ds = make_primarystall(s)
    ps = s.case[:initial_points] |> change_params
    t,theta,cm = ds(ps)
    function cost(p)
	cm_secondary = secondary_loads(t, p)
	return error_function(collect(t), cm+cm_secondary, cmd[:,1], cmd[:,3])
    end
    return cost
end

normal_error{T<:AbstractFloat}(t::Vector{T}, cl::Vector{T}, td::Vector{T}, c_data::Vector{T}) = norm( [interp(t, cl, td[i]) - c_data[i] for i in 1:length(c_data)] )/norm(c_data)
function constrained_error{T<:AbstractFloat}(phase1::T, phase2::T)
    function ce(t::Vector{T}, cl::Vector{T}, td::Vector{T}, c_data::Vector{T})
	b1 = phase1 .< t .< phase2
	b2 = phase1 .< td .< phase2
	return normal_error(t[b1], cl[b1], td[b2], c_data[b2])
    end
    return ce
end

function eta_constraint{T<:AbstractFloat}(p::Matrix{T})
    etas = p[:,3:4]
    b = etas*[1,1.44] .< 0
    return norm([1000,1000][b])
end

function radius_norm_error{T<:AbstractFloat}(x1::Array{T},y1::Array{T},x2::Array{T},y2::Array{T})
    l1,l2 = length(x1), length(x2)
    r2 = zeros(l1)
    for i in 1:l1
	x,y = x1[i], y1[i]
	r2[i] = (x-x2[1])^2+(y-y2[1])^2
	for j in 2:l2
	   a = (x-x2[j])
	   b = (y-y2[j])
	   r = a^2+b^2
	   r2[i] = r<r2[i] ? r : r2[i]
       end
    end
    return sum(r2)
end

#------------------
#utility (printing, plotting, interp, load_data, etc)
#------------------
function datafile_formatting(cmd_file::AbstractString, θ::Function, load::AbstractString, tort::AbstractString, k::AbstractFloat)
    d = open(readlines, cmd_file)
    l = length(split(strip(d[1])))
    data = zeros(length(d), l)
    for i in 1:length(d)
	data[i,:] = map(float, split(strip(d[i])))
    end
    r,c = size(data)
    if tort == "theta"
	θdata = data[:,1]
        θdata = max(θdata...)>2pi ? θdata*=pi/180 : θdata
        td = invertθ(θdata, 1.0)
    elseif tort == "t"
	td = data[:,1]
	θdata = θ(td)
	td*=k
    else
	error("Format must either be in t or theta")
    end
    cmdata = data[:,2]
    open(cmd_file, "w") do x
	write(x, "t\ttheta\t$load\n")
	for i in 1:r
	    t = td[i]
	    theta = θdata[i]
	    c = cmdata[i]
	    write(x,"$t\t$theta\t$c\n")
	end
    end
    return data
end
    
function time_formatter(d::DateTime)
       h = Dates.hour(d)
       m = Dates.minute(d)
       s = Dates.second(d)
       day = Dates.day(d)
       month = Dates.month(d)
       year = Dates.year(d)
       return "$year-$month-$day-$h-$m-$s"
end

function savedata{T<:AbstractFloat}(tsvfile::AbstractString, ps::Matrix{T}, best_cost::T, date=now())
    open(tsvfile, "a") do x
	write(x, "initial_points = $ps\t cost = $best_cost\n")
    end
end

function save_corrections{T<:AbstractFloat}(casefile::AbstractString, p::Matrix{T}, symbols)
    text = readall("$casefile.jl")
    r = r"=.*?\n"
    rextra = r"extra\[.*?\n"
    for i in 1:length(symbols)
	s = symbols[i]
	m0 = search(text, "extra[:$s]")
	if m0.start == 0
	    m2 = search(text, rextra)
	    if m2.start == 0; m2 = 1:length(text.data); end;
	    text = text[1:m2.stop] * "\nextra[:$s] = $(p[i])\n" * text[m2.stop+1:end]
	else
	    m = match(r, text, m0[1])
	    l = length(m.match)
	    text = text[1:m.offset] * " $(p[i])\n" * text[m.offset+l:end]
	end
    end
    open("$casefile.jl","w") do f
	write(f, text)
    end
end
function cyclic_data{T<:AbstractFloat}(cmd::Matrix{T})
    cmdata = cmd[:,3]
    θdata = cmd[:,2]
    td = cmd[:,1]
    return (t) -> interp(td,cmdata,(2pi+t)%2pi)
end
function shift_data(cmd_file::AbstractString, phase::AbstractFloat)
    header = open(cmd_file) do x; readline(x); end
    cmd = load_data(cmd_file)
    cmdata = cmd[:,3]
    θdata = cmd[:,2]
    td = cmd[:,1]
    if abs(phase)>2pi; phase=phase*pi/180; end
    phase = (phase+2pi) % 2pi
    p1 = [td.<phase]
    p2 = !p1
    dat1 = cmd[p2,:]
    dat2 = cmd[p1,:]
    shift = dat1[1,1]
    dat1[:,1] -= shift
    dat2[:,1] += 2pi-shift
    cmd = [dat1; dat2;]
    save_cmd_file(cmd, header, cmd_file)
    return cmd
end
function save_cmd_file(cmd, header, cmd_file)
    open(cmd_file,"w") do x
	write(x, header)
	r,c = size(cmd)
	for i in 1:r
	    t,theta,cm = cmd[i,:]
	    write(x, "$t\t$theta\t$cm\n")
	end
    end
end

function nonsense(files, dest, legend_loc)
    r = r"/[^/]*$"
    for i in 1:length(files[1:end])
	f = files[i]
	name = match(r, files[i]).match
	plotcase(f; save=true, dir = "$dest$name", legend_on = true, legend_loc = legend_loc)
    end
end

function smoother{T<:AbstractFloat}(cmd::Matrix{T}, n::Integer, harmonics::Integer)
    td = cmd[:,1]
    θdata = cmd[:,2]
    cmdata = cmd[:,3]
    xsig = linspace(min(td...), max(td...), n) |> collect
    y(t) = interp(td,cmdata,(2pi+t)%2pi)
    sig = y(xsig)
    fsig = fft(sig)
    fsig[harmonics:end-harmonics] = 0
    sig = real(ifft(fsig))
    return xsig, sig
end

function load_data(d::AbstractString, splitter)
    f = open(d); d = readlines(f); close(f)
    l = length(split(strip(d[1]), splitter))
    x = zeros(length(d), l)
    for i in 1:length(d)
	x[i,:] = map(float, split(strip(d[i]), splitter))
    end
    x
end
function load_data(d::AbstractString; check=true)
    f = open(d); d = readlines(f); close(f)
    header = d[1]
    ismatch(r"drag|moment|lift|cd|cm|cl"i, header) || error("header is WRONG: should be t, θ, load")
    d = d[2:end]
    l = length(split(strip(d[1])))
    x = zeros(length(d), l)
    for i in 1:length(d)
	x[i,:] = map(float, split(strip(d[i])))
    end
    x
end
function check_data{T<:AbstractFloat}(cmd::Matrix{T})
    r,c = size(cmd)
    passing=true
    if c != 3
	passing=false
	println("Number of data columns != 3 !")
    end
    header = cmd[1,:]
    if !ismatch(r"drag|moment|lift|cd|cm|cl"i, header)
	passing= false
	println("header is WRONG: should be t, θ, load")
    end
    cmd = cmd[2:end,:]
    t = cmd[:,1]
    θ = cmd[:,2]
    cm = cmd[:,3]
    if false in 0 .<= t .<= 2pi
	passing=false
	println("t is not between 0 and 2pi")
    end
    if false in -2pi .<=θ .<= 2pi
	passing=false
	println("θ is not between -2pi and 2pi")
    end
    passing
end
#change parameters from 1x12 to 2x6
change_params(ps) = [ps[[1],1:6]; ps[[1],7:12]]
#Linear interpolation function
interp_periodic(x,y,p) = interp(x,y,p%(x[end]-x[1]))
function interp{T<:AbstractFloat}(x::Union{StepRangeLen{T}, Vector{T}},y::Union{StepRangeLen,Vector{T}},p::AbstractFloat)
    d = x-p
    if d[1] > 0
        return y[1]
    end
    i=2
    for i in 2:length(d)
	if d[i] > 0
	    return (y[i]-y[i-1])/(x[i]-x[i-1]) * -d[i-1] + y[i-1]
	end
    end
    return (y[end] - y[end-1])/(x[end]-x[end-1]) * -d[i-1] + y[end-1]
end
interp{T<:AbstractFloat}(x::Union{StepRangeLen{T}, Vector{T}},y::Array{T,1},p::Vector{T}) = [interp(x,y,z) for z in p]

function invertθ(θdata, k)
    θ = θdata
    mn, mx = min(θdata...), max(θdata...)
    avg, amp = (mx+mn)/2, (mx-mn)/2
    t = zeros(θdata)
    ph = asin_rounded((θdata[1] - avg)/amp)
    peak = 0
    for i in 2:length(t)
	if θ[i] == mx || θ[i] == mn
	    peak+=1
	end
	t[i] = peak%2==0? pi*peak + asin_rounded((θ[i] - avg)/amp)-ph : pi*peak - asin_rounded((θ[i]-avg)/amp)-ph
    end
    return t/k
end
end

