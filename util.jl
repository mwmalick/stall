using Allstall
import SerializeCases
using PyPlot
function init_case(casefile)
    SerializeCases.serialize_case(casefile)
    casefile = casefile[1:end-3]
    return Slacker(open(deserialize, casefile))
end
function plotcase(s::Slacker, plottheta=false, clear=true, legend_on=true)
    ps = s.case[:initial_points] |> change_params
    normal_setup(s)
    ds = make_primarystall(s)
    t,theta,cm = ds(ps)
    t2 = t*180/pi
    theta2 = theta*180/pi
    @shit(s.case, (cmd, quantity))
    t_or_theta = plottheta ? theta2 : t2
    data_x_axis = plottheta ? cmd[:,2]*180/pi : cmd[:,1]*180/pi
    undersea_error = plottheta ? "Angle of Attack (degrees)" : "Phase (degrees)"
    clear && clf()
    plots = plot(t_or_theta, cm, data_x_axis, cmd[:,3])
    plots[1][:set_label]("Primary Stall")
    plots[2][:set_label]("Experimental Data")
    xlabel(undersea_error)
    ylabel("$quantity Coefficient")
    title(s.case[:plottitle])
    plottheta || xticks(collect(0:30:360))
    legend_on && legend(loc=s.case[:legendloc], fontsize=11)
    return t,theta,cm
end
plotcase(f::String, args...) = plotcase(init_case(f), args...)
plotcase(s::Slacker, ps::Matrix{T} where T<:AbstractFloat, args...) = (s.case[:initial_points] = ps; plotcase(s, args...))
function plotsave(s::Slacker, dest::String, plottheta=false, args...)
    plotcase(s, plottheta, args...)
    savename = s.case[:savename]
    word = plottheta ? "theta" : "phase"
    savefig("$dest/$savename-$word.png", format="png")
end
plotsave(f::String, args...) = plotsave(init_case(f), args...)
function plotsave(s::Slacker, ps::Matrix{T} where T<:AbstractFloat, args...)
    @assert size(ps) == (1,12)
    s.case[:initial_points] = ps
    plotsave(s, args...)
end

function plot_secondary(s::Slacker, ps::Matrix{T} where T<:AbstractFloat, plottheta=false, clear=true, legend_on=true)
    t,theta,cm = plotcase(s, plottheta, clear, legend_on)
    cm_secondary = secondary_loads(t, ps)
    arg = plottheta ? theta : t
    plots = plot(arg*180/pi, cm+cm_secondary)
    plots[1][:set_label]("Primary + Secondary Stall")
    legend_on && legend(loc = s.case[:legendloc], fontsize=11)
    plottheta || xticks(collect(0:30:360))
    return t, cm_secondary
end
plot_secondary(f::String, ps::Matrix{T} where T<:AbstractFloat, args...) = plot_secondary(init_case(f),ps,args...)
save_secondary(f::String, dest::String, ps::Matrix{T} where T<:AbstractFloat, args...) = save_secondary(init_case(f) , dest, ps,args...)
function save_secondary(s::Slacker, dest::String, ps::Matrix{T} where T<:AbstractFloat, plottheta=false, args...)
    plot_secondary(s,ps,plottheta,args...)
    savename = s.case[:savename]
    word = plottheta ? "theta" : "phase"
    savefig("$dest/$savename-$word.png", format="png")
end
    
function alpha(s::Slacker)
    @shit(s.case, (n, αs))
    @shit(s.outputs, (t, x, w, w_, ΔC, ΔC_))
    bvect = s.matrices[:bvect]
    w0(t) = w(t)[1]
    w0_(t) = w_(t)[1]
    α = zeros(length(t))
    α_ = zeros(length(t))
    for (i,ti) in enumerate(t)
        λ = x[[i],3:n+2]
        λ0 = .5(λ*bvect)[1]
        λ0_ = 0.0
        u0 = u0fun(ti)
        u0_ = u0_fun(ti)
	α[i] = (w0(ti)-λ0)/u0 - αs
	α_[i] = Allstall.α_fun(w0(ti), λ0, u0, w0_(ti), λ0_, u0_)
    end
    return α
end
