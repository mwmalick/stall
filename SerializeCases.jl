module SerializeCases
using Allstall
function serialize_case(f::AbstractString)
    code = open(readstring,f)
    prog = parse("begin $code end")
    eval(prog)
    name = match(r".*(?=\.jl)", f).match
    open(name,"w") do x
	serialize(x, case)
    end
end
function convert_cases(name::AbstractString)
    foldername = match(r"[^/]*/?$", name).match
    foldername = match(r"[^/]*", foldername).match
    files = filter(readdir(name)) do x
	ismatch(r"\.jl$", x) && contains(x, foldername)
    end
    println(files)
    for f in files
	println(f)
	serialize_case("$name/$f")
    end
end

end


