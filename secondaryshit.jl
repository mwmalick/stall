# ζ,ω,h,ti,Δt = params
include("util.jl")
cases = [
	 "mastercases/vr12/vr12-k0.1-m0.4-moment.jl"
	 "mastercases/vr12/vr12-k0.05-m0.2-moment.jl"
	 "mastercases/vr12/vr12-k0.05-m0.3-drag.jl"
	 "mastercases/vr12/vr12-k0.05-m0.3-moment.jl"
	 "mastercases/vr12/vr12-k0.05-m0.4-moment.jl"]
minpoints = [
	     .1 1 -.5 120pi/180 0
	     .1 1 -.5 150pi/180 0
	     .1 1 -.5 90pi/180 0
	     .1 1 -.5 137pi/180 0
	     .1 1 -.5 110pi/180 0]
maxpoints = [
	     100 30 .5 160pi/180 40pi/180
	     100 30 .5 170pi/180 40pi/180
	     100 30 .5 120pi/180 40pi/180
	     100 30 .5 150pi/180 40pi/180
	     100 30 .5 140pi/180 40pi/180]

N = 100
n=10
iterations = 500
function runaround(N, n, iterations, minpoint, maxpoint, case, p1, p2)
    name = match(r"[^/]*(?=\.jl)", case).match
    file = "mastercases/secondary/$name.parameters"
    psp_file = "mastercases/secondary/psp/$name.psp"
    ps = zeros(1,5)
    if isfile(file)
	ps = open(deserialize, file)
    end
    s = init_case(case)
    if isfile(psp_file) 
	s.case[:initial_points] = open(deserialize, psp_file)
    end
    normal_setup(s)
    make_primarystall(s)
    cost = secondary_cost(s, constrained_error(p1,p2))
    @time params = shotgun(cost, N, n, iterations, minpoint, maxpoint; startpoints = ps)
    params = params[[1],:]
    open(file,"w") do x
	serialize(x, params)
    end
end
#not working
function asshole(cases, minpoints, maxpoints)
    for (i,case) in enumerate(cases)
	minpoint = minpoints[i]
	maxpoint = maxpoints[i]
	runaround(N, n, iterations, minpoint, maxpoint, case)
    end
end
runaround(N, n, iterations, minpoints[[4],:], maxpoints[[4],:], cases[4], 118pi/180, 180pi/180)
