using PyPlot
using Allstall
import Allstall.interp
import SerializeCases
file = "mastercases/yawed/moment-m04/alpha8k037"
SC = SerializeCases
function initialize_case(filename::AbstractString)
    SC.serialize_case(filename)
    casefile = filename[1:end-3]
    case = open(deserialize, casefile)
    return Slacker(case)
end
file_swept = file*"swept.jl"
file_unswept = file*"unswept.jl"
s = initialize_case(file_unswept)
s2 = initialize_case(file_swept)
normal_setup(s)
normal_setup(s2)
case = s.case
k = case[:k]
cmd = case[:cmd]
cmd2 = s2.case[:cmd]
initial_points = case[:initial_points]
initial_points = [6.8433648918519 99.99992841819325 0.10000014889066962 1.7319470940978208 0.27991236302106787 -0.36140857033232415 12.059689960178355 13.2183808190065 0.39903587458876955 1.9999991767996217 0.2914538944300567 -0.7270156053242189]
initial_points = [9.999999965306317 -1.8315947648919657 1.204751717203145 1.0239274330399863 0.1476899510239781 1.999992733603091 -0.9070993311957407 9.99999989106507 0.21974791125372972 1.485985389950845 0.41884833136871474 -1.2826283273689354]	 
airfoil_name = case[:airfoil_name]
ds = make_primarystall(s)
ds2 = make_primarystall(s2)
ps = change_params(initial_points)
t,theta,cm = ds(ps)
ps_modified = copy(ps)

t2,theta2,cm2 = ds2(ps_modified)
stallangle = 10pi/180 #NACA0012 stall angle, unswept

correction = cmd2[1,3] - cmd[1,3]
clf()
plots = plot(t*180/pi, cm, cmd[:,1]*180/pi, cmd[:,3], ".-", t2*180/pi, cm2, cmd2[:,1]*180/pi, cmd2[:,3]-correction, ".-")
plots[1][:set_label]("Model, 0 degrees yaw")
plots[2][:set_label]("Data, 0 degrees yaw")
plots[3][:set_label]("Model, 30 degrees yaw")
plots[4][:set_label]("Data, 30 degrees yaw")
#plots = plot(t,cm,cmd[:,1], cmd[:,3])
xlabel("phase (degrees)")
ylabel("moment coefficent")
title(s.case[:plottitle])
legend(loc=4)
