include("util.jl")
vr12cases = split("vr12-k0.05-m0.2-moment.jl
vr12-k0.05-m0.3-moment.jl
vr12-k0.05-m0.4-moment.jl
vr12-k0.1-m0.2-moment.jl
vr12-k0.1-m0.3-moment.jl
vr12-k0.1-m0.4-moment.jl
vr12-k0.05-m0.3-drag.jl
vr12-k0.1-m0.3-drag.jl")
vr12cases = ["mastercases/vr12/$m" for m in vr12cases]
vr7cases = split("vr7-k0.05-drag.jl
vr7-k0.05-moment.jl
vr7-k0.15-drag.jl
vr7-k0.15-moment.jl
vr7-k0.1-drag.jl
vr7-k0.1-moment.jl
vr7-k0.25-drag.jl
vr7-k0.25-moment.jl
vr7-k0.2-drag.jl
vr7-k0.2-moment.jl")
vr7cases = ["mastercases/vr7/$m" for m in vr7cases]
vr7_moment_cases = freg(vr7cases, r"moment")
vr7_drag_cases = freg(vr7cases, r"drag")
ssca09cases = split("ssca09-k0.0244-m0.40.jl
ssca09-k0.0257-m0.39-a0.07.jl
ssca09-k0.0282-m0.45.jl
ssca09-k0.0505-m0.4.jl
ssca09-k0.0506-m0.4-a0.07.jl
ssca09-k0.0571-m0.35.jl")
ssca09cases = ["mastercases/ssca09/$m" for m in ssca09cases]
yawedcases = split("alpha8k037swept.jl
alpha8k037unswept.jl
alpha8k075swept.jl
alpha8k075unswept.jl
alpha8k093swept.jl
alpha8k093unswept.jl")
yawed_moment_cases = ["mastercases/yawed/moment-m04/$m" for m in yawedcases]
yawed_drag_cases = ["mastercases/yawed/drag-m04/$m" for m in yawedcases]
yawedcases = [yawed_moment_cases; yawed_drag_cases]
unsteady_naca0012_cases = [
"mastercases/favier/alpha9k135-drag/phi0.jl",
"mastercases/favier/alpha9k135-drag/phi135.jl",
"mastercases/favier/alpha9k135-drag/phi180.jl",
"mastercases/favier/alpha9k135-drag/phi225.jl",
"mastercases/favier/alpha9k135-drag/phi270.jl",
"mastercases/favier/alpha9k135-drag/phi315.jl",
"mastercases/favier/alpha9k135-drag/phi45.jl",
"mastercases/favier/alpha9k135-drag/phi90.jl",
"mastercases/favier/alpha9k135-drag/steady.jl"]

allcases = [vr12cases
	    vr7cases
	    ssca09cases
	    yawedcases
	    unsteady_naca0012_cases]

secondarycases = [
	     "mastercases/vr12/vr12-k0.1-m0.4-moment.jl"
	     "mastercases/vr12/vr12-k0.05-m0.2-moment.jl"
	     "mastercases/vr12/vr12-k0.05-m0.3-drag.jl"
	     "mastercases/vr12/vr12-k0.05-m0.3-moment.jl"
	     "mastercases/vr12/vr12-k0.05-m0.4-moment.jl"]

#VR7 single parameter set for all reduced frequencies
function vr7singleset()
    f1 = "mastercases/vr7_singleset/points"
    f2 = "mastercases/vr7_singleset/points_drag"
    @assert(isfile(f1))
    @assert(isfile(f2))
    vr7_moment_params = open(deserialize, f1)
    vr7_drag_params = open(deserialize, f2)
    for case in vr7_drag_cases
        plotsave(case, vr7_drag_params, "singleset_figures")
	plotsave(case, vr7_drag_params, "singleset_figures",  true, true, false)
    end
    for case in vr7_moment_cases
        plotsave(case, vr7_moment_params, "singleset_figures")
	plotsave(case, vr7_moment_params, "singleset_figures", true, true, false)
    end
end
function secondarystall(dir)
    for case in secondarycases
        name = match(r"[^/]*(?=\.jl)", case).match
        file = "mastercases/secondary/$name.parameters"
	psp_file = "mastercases/secondary/psp/$name.psp"
	@assert(isfile(file))
	@assert(isfile(psp_file))
        ssp = open(deserialize, file)
	psp = open(deserialize, psp_file)
	s = init_case(case)
	s.case[:initial_points] = psp
	save_secondary(s, dir, ssp, true, true, false)
	save_secondary(s, dir, ssp, false)
    end
end
numfiles() = length(freg(rd("figures"), r"\.png$")) + length(freg(rd("singleset_figures"), r"\.png$"))
testfiles() = @assert numfiles() == 2*length(allcases) + 2*length(vr7cases) #Should be 96 files

function scam() 
    return allcases[map(x->!isfile(x),allcases)]
end
function ploteverything()
    foreach(x->assert(isfile(x)), allcases)
    for c in allcases
	plotsave(c, "figures")
	plotsave(c, "figures", true, true, false)
    end
    vr7singleset()
    testfiles()
end
