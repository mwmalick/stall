include("util.jl")
function up_and_down(N, n, iterations, maxpoint, minpoint, s::Slacker)
    ds = make_shortstall(s)
    initial_points = s.case[:initial_points]
    mn = [minpoint[[1],1:6] initial_points[[1], 7:12]]
    mx = [maxpoint[[1],1:6] initial_points[[1], 7:12]]
    cost = defaultcost(s, ds, normal_error, eta_constraint)
    @time ps = shotgun(cost, N, n, iterations, mn, mx; startpoints = initial_points)
    down_params = ps[[1],1:6]
    mn = [down_params minpoint[[1], 7:12]]
    mx = [down_params maxpoint[[1], 7:12]]
    @time ps = shotgun(cost, N, n, iterations, mn, mx; startpoints = ps)
    tsvfile = s.case[:tsvfile]
    savedata(tsvfile, ps[[1],:], cost(ps[[1],:]))
    return ps, cost
end
function single_set(N, n, iterations, maxpoint, minpoint, s::Slacker; plotting=true)
    s.case[:ranges] = [-1.0, 4pi/k]
    ds = make_primarystall(s)
    mn = [minpoint[[1],1:6] ps[[1],7:12]]
    mx = [maxpoint[[1],1:6] ps[[1],7:12]]
    cost = defaultcost(s, ds, normal_error, eta_constraint)
    @time ps = shotgun(cost, N, n, iterations, mn, mx; startpoints = ps)
    ps[1,7:12] = ps[1,1:6]
    tsvfile = s.case[:tsvfile]
    savedata(tsvfile, ps[1,:], cost(ps))
    ps = change_params(ps)
    t,theta,cm = ds(ps)
    a = s.case[:plottitle]
    s.case[:plottitle] = a*"\nSame parameters for upstroke and downstroke"
    plotting && plotresult(t,theta,cm,s)
end
function secondary_optimization(N, n, iterations, maxpoint, minpoint, s::Slacker)
    cost = secondary_cost(s, normal_error)
    @time params = shotgun(cost, N, n, iterations, minpoint, maxpoint)
    params = params[1,:]
    k = s.case[:k]
end

files = ARGS
assert(!(false in map(isfile,files)))
