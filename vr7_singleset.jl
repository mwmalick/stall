include("util.jl")
all_moment_files = freg(rd("data/vr7"), r"/mccalister.*moment$")
filter!(all_moment_files) do x
    !contains(x, ".002") && !contains(x, ".02")
end
all_drag_files = freg(rd("data/vr7"), r"/mcca.*drag$")
filter!(all_drag_files) do x
    !contains(x, ".002") && !contains(x, ".02")
end
conform_params(p::Matrix{T} where T <: AbstractFloat) = (size(p) == (1,12) ? change_params(p) : p)
function plot_singleset(files, p::Matrix{T} where T <: AbstractFloat)
    for f in files
	s = init_case(f)
	figure()
	plotcase(s,p)
    end
end

function plotall(files::Array{T,1} where T<: AbstractString)
    rk = r"k0\.\d*"
    for f in files
	cmd = load_data(f)
	p, = plot(cmd[:,1], cmd[:,3])
	kval = match(rk, f).match
	p[:set_label](kval)
    end
    legend(loc=4)
end
momentcases = freg(rd("mastercases/vr7/"), r"moment.jl$")
dragcases = freg(rd("mastercases/vr7/"), r"drag.jl$")
#filter!(momentcases) do x
    #contains(x, "0.25") || contains(x, "0.05")
#end
#filter!(dragcases) do x
    #contains(x, "0.25") || contains(x, "0.05")
#end
function varmit(s::Slacker, ps::Matrix{T} where T<:AbstractFloat)
    rows,cols = size(ps)
    @assert cols == 12
    @shit(s.case, (cmd, k, plottitle, quantity))
    normal_setup(s)
    ds = make_primarystall(s)
    plot(cmd[:,1], cmd[:,3], "g-*")
    for i in 1:rows
	println(i)
	p = ps[[i],:]
	t,theta,cm = ds(p |> change_params)
	ax, = plot(t,cm)
	ax[:set_label](i)
        xlabel("Phase")
        ylabel("$quantity Coefficient")
        title(plottitle)
    end
    legend()
end
function fuckmeup(ps, r, index)
    p = r .* index
    return ps.+ p
end
#=
s = init_case(momentcases[5])
shit = s.case[:initial_points]
#shit = [0 0 1.0 0 0.0 0]
shit = [shit[[1], 1:6] shit[[1], 1:6]]
index = [0 0 0 0 1 0]
index = [index index]
r = linspace(0,2.5,8)
shit = fuckmeup(shit, r, index)
plots = varmit(s,shit)
=#
#plotting k = 0.05 through 0.25 with one parameter set
p = open(deserialize, "mastercases/vr7_singleset/points")
p2 = open(deserialize, "mastercases/vr7_singleset/points_drag")
plot_singleset(dragcases, p)

